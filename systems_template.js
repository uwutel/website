const systems = {{ systems_json|safe }};

function toggleSystemDetails(systemName) {
    const trunksDiv = document.getElementById(systemName + '-trunks');
    const blocksDiv = document.getElementById(systemName + '-blocks');

    trunksDiv.style.display = trunksDiv.style.display === 'none' ? 'block' : 'none';
    blocksDiv.style.display = blocksDiv.style.display === 'none' ? 'block' : 'none';
}

function showSystemDetails(systemName) {
    const systemDiv = document.getElementById(systemName);
    systemDiv.style.backgroundColor = '#ffcc00'; // Flashing background color

    setTimeout(function() {
        systemDiv.style.backgroundColor = '#f9f9f9'; // Reset background color after 500ms
    }, 500);
}

let element =  document.getElementById('dialing-form');
if (typeof(element) != 'undefined' && element != null)
{
    document.getElementById('dialing-form').addEventListener('submit', function(event) {
        event.preventDefault();
        const sourceSystem = document.getElementById('source-system').value;
        const destinationSystem = document.getElementById('destination-system').value;
        const extension = document.getElementById('extension').value.trim();
        
        let dialString = '';
        if (sourceSystem === destinationSystem) {
            dialString = extension;
        } else {
            const path = findPath(sourceSystem, destinationSystem, []);
            if (path) {
                const trunkDials = path.map(step => step.dial || '').join('');
                const validExtension = checkExtension(destinationSystem, extension);
                if (validExtension) {
                    dialString = trunkDials + extension;
                } else {
                    dialString = 'Invalid extension for destination system';
                }
            } else {
                dialString = 'No path available';
            }
        }
    
        document.getElementById('dialing-result').innerText = 'Dial: ' + dialString;
    });
}

function findPath(currentSystem, destinationSystem) {
    // Queue of systems to explore [system, path]
    let queue = [[currentSystem, []]];
    // Set to track visited systems
    let visited = new Set();
    visited.add(currentSystem);

    while (queue.length > 0) {
        let [system, path] = queue.shift();

        // Get trunks for the current system
        const trunks = systems[system].trunks;

        for (const trunk of trunks) {
            const nextSystem = Object.keys(trunk)[0];
            const nextPath = [...path, trunk[nextSystem]];

            // Check if the next system can be used for routing (route: true)
            if (systems[nextSystem].route !== true) {
                continue; // Skip this trunk if the next system cannot be routed through
            }

            // If we reach the destination system, return the path
            if (nextSystem === destinationSystem) {
                return nextPath;
            }

            // If next system has not been visited, add it to the queue
            if (!visited.has(nextSystem)) {
                visited.add(nextSystem);
                queue.push([nextSystem, nextPath]);
            }
        }
    }

    return null; // No path found
}

function checkExtension(destinationSystem, extension) {
    const destinationBlocks = systems[destinationSystem].blocks;
    for (const block of destinationBlocks) {
        const pattern = new RegExp('^' + block.replace(/X/g, '[0-9]') + '$');
        if (pattern.test(extension)) {
            return true;
        }
    }
    return false;
}