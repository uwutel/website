import yaml
from jinja2 import Template, Environment, FileSystemLoader
import graphviz
import json
import shutil
from pathlib import Path

Path("public/").mkdir(parents=True, exist_ok=True)



# Load YAML data
data = yaml.safe_load(open("system.yaml"))

# Transform the data to match the structure expected by the template
systems = {list(item.keys())[0]: list(item.values())[0] for item in data['systems']}

# Sort systems so that external systems come first
sorted_systems = {k: v for k, v in sorted(systems.items(), key=lambda item: item[1]['type'] == 'internal')}

systems_data = []
links = []


for system in data["systems"]:
    system_name = list(system.keys())[0]
    details = system[system_name]
    
    # Start with the original description from the YAML
    description = details.get("description", "No description provided.") + "<br>"
    
    # Add type and dial strings
    description += f"Type: {details['type']}<br>"
    if "trunks" in details:
        for trunk in details["trunks"]:
            for target_name, trunk_details in trunk.items():
                dial_string = trunk_details.get("dial", "native")
                description += f"Dial to {target_name}: {dial_string}<br>"

                # Add both directions for links
                links.append({
                    "source": system_name,
                    "target": target_name,
                    "dial": dial_string
                })
                # Add reverse link (for the other direction)
                reverse_dial_string = "native" if dial_string == "" else dial_string
                links.append({
                    "source": target_name,
                    "target": system_name,
                    "dial": reverse_dial_string
                })

    systems_data.append({
        "id": system_name,
        "name": details["name"],
        "type": details["type"],
        "description": description.strip(),
        "blocks": details.get("blocks", []),
        "directory": details.get("directory", [])
    })

# D3.js HTML template
html_template = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Systems Map</title>
    <script src="https://d3js.org/d3.v6.min.js"></script>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            overflow: auto;
        }
        .node {
            stroke: black;
            stroke-width: 1px;
        }
        .node.internal {
            fill: lightgrey;
        }
        .node.external {
            fill: lightblue;
        }
        .link {
            stroke: #999;
            stroke-opacity: 0.6;
            stroke-width: 1.5px;
        }
        text {
            font-size: 12px;
            pointer-events: none;
        }
        .tooltip {
            position: absolute;
            display: none;
            background: rgba(0, 0, 0, 0.8);
            color: white;
            padding: 5px;
            border-radius: 3px;
            font-size: 12px;
            pointer-events: none;
        }
    </style>
</head>
<body>
    <h1>Systems Overview</h1>
    <div id="tooltip" class="tooltip"></div>
    <svg id="svg"></svg>

    <script>
        const nodes = %s;
        const links = %s;

        const svg = d3.select("#svg");
        let width = window.innerWidth - 50;
        let height = window.innerHeight - 300;  // Adjust for header
        svg.attr("width", width).attr("height", height);

        const tooltip = d3.select("#tooltip");

        const simulation = d3.forceSimulation()
            .force("link", d3.forceLink().id(d => d.id).distance(150))
            .force("charge", d3.forceManyBody().strength(-500))
            .force("center", d3.forceCenter(width / 2, height / 2));

        const link = svg.append("g")
            .attr("class", "links")
            .selectAll("line")
            .data(links)
            .enter().append("line")
            .attr("class", "link");

        const node = svg.append("g")
            .attr("class", "nodes")
            .selectAll("g")
            .data(nodes)
            .enter().append("g");

        node.append("circle")
            .attr("class", d => `node ${d.type}`)
            .attr("r", 15)
            .call(d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended))
            .on("mouseover", function (event, d) {
                let directory = "<strong>Directory:</strong>";
                if (d.directory) {
                    directory = d.directory.map(item => {
                        if (item.external) {
                            return `<strong>External Directory:</strong> <a href="${item.external}" target="_blank">${item.external}</a>`;
                        } else {
                            return `<br>${Object.keys(item)[0]}: ${item[Object.keys(item)[0]].join(", ")}`;
                        }
                    }).join("<br>");
                }
                tooltip.style("display", "block")
                    .html(`<strong>${d.name}</strong><br>${d.description}<br><strong>Blocks:</strong> ${d.blocks.join(", ")}<br>${directory}`);
            })
            .on("mousemove", function (event) {
                tooltip.style("top", (event.pageY + 10) + "px")
                    .style("left", (event.pageX + 10) + "px");
            })
            .on("mouseout", function () {
                tooltip.style("display", "none");
            });

        node.append("text")
            .attr("x", 20)
            .attr("y", 5)
            .text(d => d.name);

        simulation.nodes(nodes).on("tick", ticked);
        simulation.force("link").links(links);

        function ticked() {
            link.attr("x1", d => d.source.x)
                .attr("y1", d => d.source.y)
                .attr("x2", d => d.target.x)
                .attr("y2", d => d.target.y);

            node.attr("transform", d => `translate(${d.x},${d.y})`);
        }

        function dragstarted(event, d) {
            if (!event.active) simulation.alphaTarget(0.3).restart();
            d.fx = d.x;
            d.fy = d.y;
        }

        function dragged(event, d) {
            d.fx = event.x;
            d.fy = event.y;
        }

        function dragended(event, d) {
            if (!event.active) simulation.alphaTarget(0);
            d.fx = null;
            d.fy = null;
        }

        // Adjust the SVG size on window resize
        window.addEventListener("resize", function () {
            width = window.innerWidth - 50;
            height = window.innerHeight - 300;
            console.log(width)
            console.log(height)
            svg.attr("width", width).attr("height", height);
            simulation.force("center", d3.forceCenter(width / 2, height / 2));
            simulation.alpha(1).restart();
        });
    </script>
</body>
</html>
"""

html_content = html_template % (json.dumps(systems_data), json.dumps(links))

# Generate the map using Graphviz
#dot = graphviz.Digraph(comment='Systems Map', format='svg')
#dot.attr(rankdir='LR')

# Add nodes and edges with styles
#for system_name, system_details in sorted_systems.items():
#    if system_details['type'] == 'external':
#        dot.node(system_name, system_details['name'], shape='box', style='filled', color='lightblue')
#    else:
#        dot.node(system_name, system_details['name'], shape='ellipse', style='filled', color='lightgrey')
#
#    for trunk in system_details['trunks']:
#        for trunk_name, trunk_details in trunk.items():
#            dot.edge(system_name, trunk_name, label=trunk_details['dial'], fontsize='10', color='black')

# Render the graph to a file
#dot.render('systems_map')

# Convert systems data to JSON string for JavaScript
systems_json = json.dumps(systems)

# Setup Jinja2 environment
env = Environment(loader=FileSystemLoader('.'))
template = env.get_template('systems_template.html')

# Render the template with sorted data and systems JSON for JavaScript
html_output = template.render(systems=sorted_systems, systems_json=systems_json)

# Write the output to an HTML file
with open('public/systems_overview.html', 'w') as file:
    file.write(html_content + html_output)


# Setup Jinja2 environment
env = Environment(loader=FileSystemLoader('.'))
template = env.get_template('systems_template.js')

# Render the template with sorted data and systems JSON for JavaScript
html_output = template.render(systems=sorted_systems, systems_json=systems_json)

# Write the output to an HTML file
with open('public/system.js', 'w') as file:
    file.write(html_output)


# Setup Jinja2 environment
env = Environment(loader=FileSystemLoader('.'))
template = env.get_template('dir_template.html')

# Render the template with the data
html_output = template.render(systems=sorted_systems)

# Save the HTML content to a file
with open('public/directory.html', 'w') as file:
    file.write(html_output)


print("HTML file with map and calculator has been generated successfully!")

shutil.copy("index.html", "public/")
shutil.copy("nyaaaa.css", "public/")
shutil.copy("directory.js", "public/")