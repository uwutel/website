

function updateDials(sourceSystem) {
    const directoryItems = document.querySelectorAll('.directory-item');

    directoryItems.forEach(item => {
        const extension = item.getAttribute('data-extension');
        const destinationSystem = item.getAttribute('data-system');
        
        let dialString = '';
        if (sourceSystem === destinationSystem) {
            dialString = extension;
        } else {
            const path = findPath(sourceSystem, destinationSystem);
            if (path) {
                const trunkDials = path.map(step => step.dial || '').join('');
                const validExtension = checkExtension(destinationSystem, extension);
                if (validExtension) {
                    dialString = trunkDials + extension;
                } else {
                    dialString = 'Invalid extension';
                }
            } else {
                dialString = 'No path available';
            }
        }
        const dialCodeElement = item.querySelector('.dial-code');
        if (dialCodeElement) {
            dialCodeElement.textContent = dialString;
        }
    });
}